#!/bin/bash

run_user=`whoami`
if [ 'X'$run_user != 'X''root' ]
then
    echo 'Error! should run me with root...'
    exit 1
fi

cnt=`ps aux | grep oracle | grep -v $0 | grep -v 'grep' | wc -l`
if [ $cnt -gt 0 ]
then
    echo "ERROR! oracle may be running, exit..."
    exit 10
fi

# absolute path which has oracle 11gR2 archive zip file in
ARCHIVE_PATH=''

# absolute base path to install oracle, all oracle dir will be created in it
# may be a disk with enough space 
ORACLE_BASE_PATH=''


usage() { 
  echo -e "
Install oracle 11gR2
Usage:
  $0 [-f ARCHIVE_TARBARR_PATH] [-h INSTALL_HOME]
Common options:
  -d,  path to search archive zips, download and put linux.x64_11gR2_database_1of2.zip and linux.x64_11gR2_database_2of2.zip in this dir 
  -h,  absolute path to install oracle, may be a disk with enough space, default to "'`/home/oracle`'"
" 1>&2; exit 1;
}

while getopts ":d:h:" o; do
    case "${o}" in
        d)
            v=${OPTARG}
            if [ 'X'$v == 'X' ]
            then
              usage
            else
              ARCHIVE_PATH=${OPTARG}
            fi
            ;;
        h)
            v=${OPTARG}
            if [ 'X'$v == 'X' ]
            then
              ORACLE_BASE_PATH='/home/oracle'
            else
              ORACLE_BASE_PATH=${OPTARG}
            fi
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -f ${ARCHIVE_PATH}/linux.x64_11gR2_database_1of2.zip -a -f ${ARCHIVE_PATH}/linux.x64_11gR2_database_2of2.zip ]
then
    echo "searched linux.x64_11gR2_database_1of2.zip and linux.x64_11gR2_database_2of2.zip in ${ARCHIVE_PATH}"
else
    echo "ERROR! can not find linux.x64_11gR2_database_1of2.zip and linux.x64_11gR2_database_2of2.zip in ${ARCHIVE_PATH}, exit..."
    exit 10
fi


if [ 'X'${ORACLE_BASE_PATH} == 'X' ]
then
    ORACLE_BASE_PATH='/home/oracle'
    echo "use default oracle base path: ${ORACLE_BASE_PATH}"    
fi
if [ -d ${ORACLE_BASE_PATH} ]
then
    echo "Error! oracle base dir ${ORACLE_BASE_PATH} exists, should clean oracle path before re-install..."
    exit 5
fi

mkdir -p ${ORACLE_BASE_PATH}


is_centos=`awk -F '=' '/PRETTY_NAME/ { print $2 }' /etc/os-release | grep -i centos | wc -l`
# is_ubuntu=`awk -F '=' '/PRETTY_NAME/ { print $2 }' /etc/os-release | grep -i ubuntu | wc -l`

###### Package Requirements Begin
# binutils-2.23.52.0.1-12.el7.x86_64 
# compat-libcap1-1.10-3.el7.x86_64 
# compat-libstdc++-33-3.2.3-71.el7.i686
# compat-libstdc++-33-3.2.3-71.el7.x86_64
# gcc-4.8.2-3.el7.x86_64 
# gcc-c++-4.8.2-3.el7.x86_64 
# glibc-2.17-36.el7.i686 
# glibc-2.17-36.el7.x86_64 
# glibc-devel-2.17-36.el7.i686 
# glibc-devel-2.17-36.el7.x86_64 
# ksh
# libaio-0.3.109-9.el7.i686 
# libaio-0.3.109-9.el7.x86_64 
# libaio-devel-0.3.109-9.el7.i686 
# libaio-devel-0.3.109-9.el7.x86_64 
# libgcc-4.8.2-3.el7.i686 
# libgcc-4.8.2-3.el7.x86_64 
# libstdc++-4.8.2-3.el7.i686 
# libstdc++-4.8.2-3.el7.x86_64 
# libstdc++-devel-4.8.2-3.el7.i686 
# libstdc++-devel-4.8.2-3.el7.x86_64 
# libXi-1.7.2-1.el7.i686 
# libXi-1.7.2-1.el7.x86_64 
# libXtst-1.2.2-1.el7.i686 
# libXtst-1.2.2-1.el7.x86_64 
# make-3.82-19.el7.x86_64 
# sysstat-10.1.5-1.el7.x86_64 
###### Package Requirements End

install_libs_centos(){
    yum update -y
    yum install -y unzip dos2unix
    yum install -y binutils elfutils-libelf elfutils-libelf-devel compat-libcap1 compat-libstdc++-33 gcc \
      glibc glibc.i686 glibc-devel glibc-devel.i686 \
      ksh libaio libaio-devel libgcc libstdc++ libstdc++-devel libXi libXtst \
      unixODBC unixODBC-devel make sysstat 
}

# install_libs_ubuntu(){
#     apt-get update -y
#     apt-get install -y unzip dos2unix
#     apt-get install -y  build-essential
#     apt-get install -y binutils elfutils libelf1 libelf-dev ksh libaio1 libaio-dev expat libxi6 libxext6 \
#       unixodbc unixodbc-dev make sysstat
# }

if [ $is_centos -gt 0 ]
then
    # install some libs
    install_libs_centos
fi

# if [ $is_ubuntu -gt 0 ]
# then
#     # install some libs
#     install_libs_ubuntu
# fi

# Disabling Transparent HugePages
r=`cat /sys/kernel/mm/transparent_hugepage/enabled | grep '\[never\]' | wc -l`
if [ $r -lt 1 ]
then
    r1=`cat /etc/default/grub | grep 'transparent_hugepage' | wc -l`
    if [ $r1 -lt 1 ]
    then
        sed -i 's/quiet/quiet transparent_hugepage=never numa=off/' /etc/default/grub
        grub2-mkconfig -o /boot/grub2/grub.cfg
    fi

    echo never > /sys/kernel/mm/transparent_hugepage/enabled
    echo "transparent hugepage temporarily disabled, and will be disabled after reboot"
fi


# # Confirm Host Name Resolution
# ping -c 5 `hostname`
# r=$?
# echo $r


groupadd oinstall
groupadd oracle
groupadd dba
groupadd oper
groupadd asmadmin
groupadd asmdba
groupadd asmoper

useradd -g oinstall -G dba,oper,asmdba -s /bin/bash oracle
echo oracle:oracle.123 | chpasswd
echo 'you can use `su - oracle` to login as user `oracle`, and password is: `oracle.123`'


# Change Resource Limits for the Oracle Software Installation Users
###### Resource Limits Begin
# oracle          soft      nofile  1024
# oracle          hard      nofile  65536
# oracle          soft      nproc   2047
# oracle          hard      nproc   16384
# oracle          soft      stack   10240
# oracle          hard      stack   10240
###### Resource Limits End

cnt=`cat /etc/security/limits.conf | grep -E "^\s*oracle\s+soft\s+nofile" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|oracle\s+soft\s+nofile\s+\w+|oracle        soft    nofile    1024|g" /etc/security/limits.conf
else
    echo "oracle        soft    nofile    1024" >> /etc/security/limits.conf
fi

cnt=`cat /etc/security/limits.conf | grep -E "^\s*oracle\s+hard\s+nofile" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|oracle\s+hard\s+nofile\s+\w+|oracle        hard    nofile    65536|g" /etc/security/limits.conf
else
    echo "oracle        hard    nofile    65536" >> /etc/security/limits.conf
fi

cnt=`cat /etc/security/limits.conf | grep -E "^\s*oracle\s+soft\s+nproc" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|oracle\s+soft\s+nproc\s+\w+|oracle        soft    nproc    2047|g" /etc/security/limits.conf
else
    echo "oracle        soft    nproc    2047" >> /etc/security/limits.conf
fi

cnt=`cat /etc/security/limits.conf | grep -E "^\s*oracle\s+hard\s+nproc" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|oracle\s+hard\s+nproc\s+\w+|oracle        hard    nproc    16384|g" /etc/security/limits.conf
else
    echo "oracle        hard    nproc    16384" >> /etc/security/limits.conf
fi

cnt=`cat /etc/security/limits.conf | grep -E "^\s*oracle\s+soft\s+stack" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|oracle\s+soft\s+stack\s+\w+|oracle        soft    stack    10240|g" /etc/security/limits.conf
else
    echo "oracle        soft    stack    10240" >> /etc/security/limits.conf
fi

cnt=`cat /etc/security/limits.conf | grep -E "^\s*oracle\s+hard\s+stack" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|oracle\s+hard\s+stack\s+\w+|oracle        hard    stack    10240|g" /etc/security/limits.conf
else
    echo "oracle        hard    stack    10240" >> /etc/security/limits.conf
fi


# Configuring Kernel Parameters for Linux
###### Kernel Parameters Begin
# fs.aio-max-nr = 1048576
# fs.file-max = 6815744
# kernel.shmall = 2097152
# # bytes, on 64-bit Linux Systems, Recommended: More than half the physical memory
# kernel.shmmax = 4294967295
# kernel.shmmni = 4096
# kernel.sem = 250 32000 100 128
# net.ipv4.ip_local_port_range = 9000 65500
# net.core.rmem_default = 262144
# net.core.rmem_max = 4194304
# net.core.wmem_default = 262144
# net.core.wmem_max = 1048576
###### Kernel Parameters End

# fs.aio-max-nr
cnt=`cat /etc/sysctl.conf | grep -E "^\s*fs.aio-max-nr" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|fs.aio-max-nr.+|fs.aio-max-nr = 1048576|g" /etc/sysctl.conf
else
    echo "fs.aio-max-nr = 1048576" >> /etc/sysctl.conf
fi
# fs.file-max
cnt=`cat /etc/sysctl.conf | grep -E "^\s*fs.file-max" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|fs.file-max.+|fs.file-max = 6815744|g" /etc/sysctl.conf
else
    echo "fs.file-max = 6815744" >> /etc/sysctl.conf
fi
# kernel.shmall
cnt=`cat /etc/sysctl.conf | grep -E "^\s*kernel.shmall" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|kernel.shmall.+|kernel.shmall = 2097152|g" /etc/sysctl.conf
else
    echo "kernel.shmall = 2097152" >> /etc/sysctl.conf
fi
# kernel.shmmax
# bytes, on 64-bit Linux Systems, Recommended: More than half the physical memory
bytes=`free -b | grep Mem | awk '{print $2}'`
r=`echo $bytes/2+1|bc`
cnt=`cat /etc/sysctl.conf | grep -E "^\s*kernel.shmmax" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|kernel.shmmax.+|kernel.shmmax = $r|g" /etc/sysctl.conf
else
    echo "kernel.shmmax = $r" >> /etc/sysctl.conf
fi
# kernel.shmmni
cnt=`cat /etc/sysctl.conf | grep -E "^\s*kernel.shmmni" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|kernel.shmmni.+|kernel.shmmni = 4096|g" /etc/sysctl.conf
else
    echo "kernel.shmmni = 4096" >> /etc/sysctl.conf
fi
# kernel.sem
cnt=`cat /etc/sysctl.conf | grep -E "^\s*kernel.sem" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|kernel.sem.+|kernel.sem = 250 32000 100 128|g" /etc/sysctl.conf
else
    echo "kernel.sem = 250 32000 100 128" >> /etc/sysctl.conf
fi
# net.ipv4.ip_local_port_range
cnt=`cat /etc/sysctl.conf | grep -E "^\s*net.ipv4.ip_local_port_range" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|net.ipv4.ip_local_port_range.+|net.ipv4.ip_local_port_range = 9000 65500|g" /etc/sysctl.conf
else
    echo "net.ipv4.ip_local_port_range = 9000 65500" >> /etc/sysctl.conf
fi
# net.core.rmem_default
cnt=`cat /etc/sysctl.conf | grep -E "^\s*net.core.rmem_default" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|net.core.rmem_default.+|net.core.rmem_default = 262144|g" /etc/sysctl.conf
else
    echo "net.core.rmem_default = 262144" >> /etc/sysctl.conf
fi
# net.core.rmem_max
cnt=`cat /etc/sysctl.conf | grep -E "^\s*net.core.rmem_max" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|net.core.rmem_max.+|net.core.rmem_max = 4194304|g" /etc/sysctl.conf
else
    echo "net.core.rmem_max = 4194304" >> /etc/sysctl.conf
fi
# net.core.wmem_default
cnt=`cat /etc/sysctl.conf | grep -E "^\s*net.core.wmem_default" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|net.core.wmem_default.+|net.core.wmem_default = 262144|g" /etc/sysctl.conf
else
    echo "net.core.wmem_default = 262144" >> /etc/sysctl.conf
fi
# net.core.wmem_max
cnt=`cat /etc/sysctl.conf | grep -E "^\s*net.core.wmem_max" | wc -l`
if [ $cnt -gt 0 ]
then
    sed -i -r "s|net.core.wmem_max.+|net.core.wmem_max = 1048576|g" /etc/sysctl.conf
else
    echo "net.core.wmem_max = 1048576" >> /etc/sysctl.conf
fi

sysctl -p


# Identifying Required Software Directories
MOUNT_POINT=${ORACLE_BASE_PATH}

# Creating an Oracle Base Directory
mkdir -p ${MOUNT_POINT}/app/oracle
chown -R oracle:oinstall ${MOUNT_POINT}/app/oracle
chmod -R 775 ${MOUNT_POINT}/app/oracle
ORACLE_BASE=${MOUNT_POINT}/app/oracle

# oracle inventory
mkdir -p ${MOUNT_POINT}/app/oraInventory
chown -R oracle:oinstall ${MOUNT_POINT}/app/oraInventory
chmod -R 775 ${MOUNT_POINT}/app/oraInventory

# oracle home
mkdir -p ${ORACLE_BASE}/product/11gr2/dbhome_1
ORACLE_HOME=${ORACLE_BASE}/product/11gr2/dbhome_1

chown -R oracle:oinstall ${ORACLE_BASE}/product
chmod -R 775 ${ORACLE_BASE}/product

# oracle data
mkdir ${ORACLE_BASE}/oradata
chown -R oracle:oinstall ${ORACLE_BASE}/oradata
chmod -R 775 ${ORACLE_BASE}/oradata

# oracle fast_recovery
mkdir ${ORACLE_BASE}/fast_recovery_area
chown -R oracle:oinstall ${ORACLE_BASE}/fast_recovery_area
chmod -R 775 ${ORACLE_BASE}/fast_recovery_area


# Configuring Oracle Software Owner Environment
echo "umask 022" >> /home/oracle/.bash_profile
echo "export ORACLE_BASE=${MOUNT_POINT}/app/oracle" >> /home/oracle/.bash_profile
echo "export ORACLE_HOME=${ORACLE_BASE}/product/11gr2/dbhome_1" >> /home/oracle/.bash_profile
echo "export ORACLE_SID=orcl" >> /home/oracle/.bash_profile

# Setting the ORACLE_HOSTNAME Environment Variable
echo "export ORACLE_HOSTNAME=`hostname`" >> /home/oracle/.bash_profile

ORACLE_HOSTNAME=`hostname`
ORACLE_SID=orcl

if [ -d ${ORACLE_BASE}/tmp_packages ]
then
    rm -rf ${ORACLE_BASE}/tmp_packages
fi
mkdir -p ${ORACLE_BASE}/tmp_packages


mv ${ARCHIVE_PATH}/linux.x64_11gR2_database_1of2.zip ${ORACLE_BASE}/tmp_packages
mv ${ARCHIVE_PATH}/linux.x64_11gR2_database_2of2.zip ${ORACLE_BASE}/tmp_packages

cd ${ORACLE_BASE}/tmp_packages
unzip ${ORACLE_BASE}/tmp_packages/linux.x64_11gR2_database_1of2.zip
unzip ${ORACLE_BASE}/tmp_packages/linux.x64_11gR2_database_2of2.zip
rm -rf ${ORACLE_BASE}/tmp_packages/linux.x64_11gR2_database_1of2.zip
rm -rf ${ORACLE_BASE}/tmp_packages/linux.x64_11gR2_database_2of2.zip

chown -R oracle:oinstall ${ORACLE_BASE}/tmp_packages
chmod -R 775 ${ORACLE_BASE}/tmp_packages

cd ${ORACLE_BASE}/tmp_packages/database/response

dos2unix *

sed -i "s/oracle.install.option=/oracle.install.option=INSTALL_DB_AND_CONFIG/g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
ORACLE_HOSTNAME=`hostname`
sed -i "s/ORACLE_HOSTNAME=/ORACLE_HOSTNAME=${ORACLE_HOSTNAME}/g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s/UNIX_GROUP_NAME=/UNIX_GROUP_NAME=oinstall/g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s|INVENTORY_LOCATION=|INVENTORY_LOCATION=${MOUNT_POINT}/app/oraInventory|g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s|SELECTED_LANGUAGES=|SELECTED_LANGUAGES=en,zh_CN|g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s|ORACLE_HOME=|ORACLE_HOME=${ORACLE_HOME}|g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s|ORACLE_BASE=|ORACLE_BASE=${ORACLE_BASE}|g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s|oracle.install.db.InstallEdition=|oracle.install.db.InstallEdition=SE|g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s|oracle.install.db.DBA_GROUP=|oracle.install.db.DBA_GROUP=dba|g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s|oracle.install.db.OPER_GROUP=|oracle.install.db.OPER_GROUP=oper|g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s|oracle.install.db.config.starterdb.type=|oracle.install.db.config.starterdb.type=GENERAL_PURPOSE|g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s|oracle.install.db.config.starterdb.globalDBName=|oracle.install.db.config.starterdb.globalDBName=orcl|g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s|oracle.install.db.config.starterdb.SID=|oracle.install.db.config.starterdb.SID=orcl|g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s|oracle.install.db.config.starterdb.memoryLimit=|oracle.install.db.config.starterdb.memoryLimit=512|g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s|oracle.install.db.config.starterdb.password.ALL=|oracle.install.db.config.starterdb.password.ALL=Oracle.123456|g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s|oracle.install.db.config.starterdb.storageType=|oracle.install.db.config.starterdb.storageType=FILE_SYSTEM_STORAGE|g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s|oracle.install.db.config.starterdb.fileSystemStorage.dataLocation=|oracle.install.db.config.starterdb.fileSystemStorage.dataLocation=${ORACLE_BASE}/oradata|g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s|oracle.install.db.config.starterdb.fileSystemStorage.recoveryLocation=|oracle.install.db.config.starterdb.fileSystemStorage.recoveryLocation=${ORACLE_BASE}/fast_recovery_area|g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp
sed -i "s|DECLINE_SECURITY_UPDATES=|DECLINE_SECURITY_UPDATES=true|g" ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp


echo "oracle.install.db.config.starterdb.password.ALL is: Oracle.123456, it can be supplied for the following four schemas in the starter database:"
echo "SYS  SYSTEM  SYSMAN (used by Enterprise Manager)  DBSNMP (used by Enterprise Manager)"

# use user oracle to install and init database
su - oracle <<EOF

echo "will use Oracle Universal Installer to install the oracle 11gR2..."

${ORACLE_BASE}/tmp_packages/database/runInstaller -silent -ignoreSysPrereqs -ignorePrereq  -responseFile ${ORACLE_BASE}/tmp_packages/database/response/db_install.rsp

EOF


sleep 10

# wait for oracle installer completed
while ((1 == 1))
do
    cnt=`cat ${MOUNT_POINT}/app/oraInventory/logs/installActions*.log | grep "INFO: Successfully executed the flow in SILENT mode" | wc -l`

    if [ $cnt -lt 1 ]
    then
        echo "oracle installer is running, wait..."
        sleep 15
    else
        break
    fi
done
# sleep more time to wait oracle installer completed
sleep 10

# if [ $is_ubuntu -gt 0 ]
# then
#     sed -i "s|AWK=/bin/awk|AWK=/usr/bin/awk|g" ${MOUNT_POINT}/app/oraInventory/orainstRoot.sh
# fi

${MOUNT_POINT}/app/oraInventory/orainstRoot.sh
${ORACLE_HOME}/root.sh

echo 'oracle install completed and will auto startup, use `su - oracle` and `'${ORACLE_HOME}'/bin/lsnrctl status` to check the oracle status'

rm -rf ${ORACLE_BASE}/tmp_packages

echo 'to use the database, like: '
echo "
su - oracle
${ORACLE_HOME}/bin/sqlplus / as sysdba
# can start oracle in the sqlplus prompt
connect / as sysdba
startup

# run sql in the sqlplus prompt, for example:
create user TEST identified by "'"TEST.123"'";
grant connect,resource,dba to TEST;
"
