
一批方便linux系统上部署常用软件的脚本，已在centos 7、ubuntu 18.04系统上测试通过，目前支持：
- mysql 5.7/8
- postgresql 14
- oracle 11gR2 (centos 7通过)
- nginx
- redis
- jdk
- maven
- nodejs
- php

使用方式简单，以mysql为例：
```shell
./mysql_install.sh --help
```

则给出使用提示：
```plain
Usage:
  ./mysql_install.sh -f ARCHIVE_TARBARR_PATH [-c CONFIGURATION_PATH]
Common options:
  -f,  file, absolute path of archive tarball. Without -f options will download tarball from Internet
  -c,  conf, absolute path of configuration file, default to `/tmp/mysql_env.config`
```

使用-f option指定本地mysql-server的tar包绝对路径；使用-c option可以指定自定义配置文件路径，读取自定义配置参数，脚本将自动应用到mysql配置文件中

脚本只会执行软件的基本安装和必要的配置，并不会修改过多配置参数，可以进行更多自定义操作
