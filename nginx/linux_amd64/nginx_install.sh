#!/bin/bash

run_user=`whoami`

if [ 'X'$run_user != 'X''root' ]
then
    echo 'Error! should run me with root...'
    exit 1
fi

# absolute path of nginx tarball used. if not give, will download nginx tarball from Internet
TARBALL_PATH=''

# absolute path to install nginx
INSTALL_HOME=''


usage() { 
  echo -e "
Usage:
  $0 [-f ARCHIVE_TARBARR_PATH] [-h INSTALL_HOME]
Common options:
  -f,  file, absolute path of archive tarball. Without -f options will download tarball from Internet
  -h,  home, absolute path of php install path, default to "'`/usr/local/nginx`'"
" 1>&2; exit 1;
}

while getopts ":f:h:" o; do
    case "${o}" in
        f)
            v=${OPTARG}
            if [ 'X'$v == 'X' ]
            then
              usage
            else
              TARBALL_PATH=${OPTARG}
            fi
            ;;
        h)
            v=${OPTARG}
            if [ 'X'$v == 'X' ]
            then
              usage
            else
              INSTALL_HOME=${OPTARG}
            fi
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))


if [ 'X'${TARBALL_PATH} == 'X' ]
then
    # version used when no tarball give
    DEFAULT_VERSION=1.20.2

    tarball="nginx-${DEFAULT_VERSION}.tar.gz"
    TARBALL_PATH="/tmp/${tarball}"

    if [ -f ${TARBALL_PATH} ]
    then
        rm -rf ${TARBALL_PATH}
    fi
    echo "not give nginx tarball absolute path, will download ${tarball} from Internet"
    # http://nginx.org/download/nginx-1.20.2.tar.gz
    curl http://nginx.org/download/${tarball} --output ${TARBALL_PATH}
fi
if [ ! -f ${TARBALL_PATH} ]
then
    echo "ERROR! nginx tarball:${TARBALL_PATH} not found, exit..."
    exit 11
fi

if [ 'X'${INSTALL_HOME} == 'X' ]
then
    INSTALL_HOME='/usr/local/nginx'
    echo "not give nginx install home absolute path, will use default path: ${INSTALL_HOME}"
fi
if [ -d ${INSTALL_HOME} ]
then
    echo "ERROR! nginx home:${INSTALL_HOME} exists, exit..."
    exit 10
fi


remove_nginx_centos(){
    cd /
    # stop nginx process
    systemctl disable nginx.service 
    systemctl stop nginx.service 
    
    # remove nginx* with yum
    yum remove *nginx* -y

    rm -f /etc/systemd/system/nginx.service
    systemctl daemon-reload
    
    # del previous nginx user
    userdel nginx
    groupdel nginx
}

remove_nginx_ubuntu(){
    cd /
    # stop nginx process
    systemctl disable nginx.service 
    systemctl stop nginx.service 
    
    # remove nginx* with apt-get
    apt-get remove *nginx* -y

    rm -f /etc/systemd/system/nginx.service
    systemctl daemon-reload
    
    # del previous nginx user
    userdel nginx
    groupdel nginx
}

install_libs_centos(){
    cd /
    yum update -y
    # install gcc, g++ and make
    yum install gcc gcc-c++ make -y
    
    yum install pcre -y
    yum install pcre-devel -y
    yum install zlib-devel -y
    yum install openssl openssl-devel -y 
}

install_libs_ubuntu(){
    cd /
    apt-get update -y
    # install gcc, g++ and make
    apt-get install build-essential -y 
    # apt-get install make -y 

    apt-get install libpcre3 -y 
    apt-get install libpcre3-dev -y
    apt-get install zlib1g-dev -y
    apt-get install openssl libssl-dev  -y 
}

is_centos=`awk -F '=' '/PRETTY_NAME/ { print $2 }' /etc/os-release | grep -i centos | wc -l`
is_ubuntu=`awk -F '=' '/PRETTY_NAME/ { print $2 }' /etc/os-release | grep -i ubuntu | wc -l`


if [ $is_centos -gt 0 ]
then
    # stop nginx process, and del previous nginx user
    remove_nginx_centos
    # install libs
    install_libs_centos
    # create user nginx:nginx
    adduser nginx --system --no-create-home --shell /bin/false --user-group
fi

if [ $is_ubuntu -gt 0 ]
then
    # stop nginx process, and del previous nginx user
    remove_nginx_ubuntu
    # install libs
    install_libs_ubuntu
    # create user nginx:nginx
    adduser nginx --system --no-create-home --shell /bin/false --group
fi


cd /tmp
if [ -d /tmp/nginx_packages ]
then
    rm -rf /tmp/nginx_packages
fi

mkdir -p /tmp/nginx_packages

if [ -d ${INSTALL_HOME} ]
then
    rm -rf ${INSTALL_HOME}
fi
mkdir -p ${INSTALL_HOME}

chmod -R 755 ${INSTALL_HOME}

cd /tmp/nginx_packages
tar -xzvf ${TARBALL_PATH} -C /tmp/nginx_packages --strip-components 1

make distclean

./configure \
--prefix=/etc/nginx \
--sbin-path=${INSTALL_HOME}/sbin/nginx \
--pid-path=/var/run/nginx.pid \
--lock-path=/var/run/nginx.lock \
--conf-path=/etc/nginx/nginx.conf \
--modules-path=/etc/nginx/modules \
--error-log-path=/var/log/nginx/error.log \
--http-log-path=/var/log/nginx/access.log \
--user=nginx \
--group=nginx \
--with-http_ssl_module \
--with-mail \
--with-mail_ssl_module \
--with-stream \
--with-stream_ssl_module \
--with-stream_ssl_preread_module \
--with-poll_module \
--with-http_stub_status_module \
--with-http_sub_module

make install

# exit with error if make nginx fail
ret=$?

if [ 'X'$ret != 'X'0 ]
then
    make distclean
    rm -rf /tmp/nginx_packages
    echo 'Error! make nginx failed...'
    exit 9
fi

rm -rf /tmp/nginx_packages

# add ln for "nginx"
if [ -e /usr/bin/nginx -o -L /usr/bin/nginx ]
then
    rm -rf /usr/bin/nginx
fi
ln -s ${INSTALL_HOME}/sbin/nginx /usr/bin/nginx


echo "
[Unit]
Description=The nginx HTTP and reverse proxy server
After=syslog.target network-online.target remote-fs.target nss-lookup.target
Wants=network-online.target

[Service]
Type=forking
PIDFile=/var/run/nginx.pid
# Nginx will fail to start if /var/run/nginx.pid already exists but has the wrong
# SELinux context. This might happen when running `nginx -t` from the cmdline.
# https://bugzilla.redhat.com/show_bug.cgi?id=1268621
ExecStartPre=/bin/rm -f /var/run/nginx.pid
ExecStartPre=${INSTALL_HOME}/sbin/nginx -t
ExecStart=${INSTALL_HOME}/sbin/nginx
ExecReload=/bin/kill -s HUP $MAINPID
KillSignal=SIGQUIT
TimeoutStopSec=5
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/nginx.service


rm -rf /tmp/nginx_packages

echo "nginx installed in ${INSTALL_HOME}, conf file in /etc/nginx and log in/var/log/nginx"

systemctl daemon-reload
systemctl enable nginx.service 
systemctl start nginx

echo "nginx start and will automatically start on boot, use 'systemctl status nginx' to check"