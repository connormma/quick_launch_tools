#!/bin/bash

run_user=`whoami`
if [ 'X'$run_user != 'X''root' ]
then
    echo 'Error! should run me with root...'
    exit 1
fi

# absolute path of nodejs tarball used. if not give, will download nodejs tarball from Internet
TARBALL_PATH=''

# absolute path to install nodejs
INSTALL_HOME=''


usage() { 
  echo -e "
Install nodejs and cnpm
Usage:
  $0 [-f ARCHIVE_TARBARR_PATH] [-h INSTALL_HOME]
Common options:
  -f,  file, absolute path of archive tarball. Without -f options will download tarball from Internet
  -h,  home, absolute path of jdk install path, default to "'`/usr/local/lib/nodejs`'"
" 1>&2; exit 1;
}

while getopts ":f:h:" o; do
    case "${o}" in
        f)
            v=${OPTARG}
            if [ 'X'$v == 'X' ]
            then
              usage
            else
              TARBALL_PATH=${OPTARG}
            fi
            ;;
        h)
            v=${OPTARG}
            if [ 'X'$v == 'X' ]
            then
              usage
            else
              INSTALL_HOME=${OPTARG}
            fi
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))


if [ 'X'${TARBALL_PATH} == 'X' ]
then
    # version used when no tarball give
    DEFAULT_VERSION=v16.18.0
    # distro used when no tarball give
    DEFAULT_DISTRO=linux-x64

    tarball="node-${DEFAULT_VERSION}-${DEFAULT_DISTRO}.tar.xz"
    TARBALL_PATH="/tmp/${tarball}"
    if [ -f ${TARBALL_PATH} ]
    then
        rm -rf ${TARBALL_PATH}
    fi
    echo "not give nodejs tarball absolute path, will download ${tarball} from Internet"
    # https://nodejs.org/dist/v16.18.0/node-v16.18.0-linux-x64.tar.xz
    curl https://nodejs.org/dist/${DEFAULT_VERSION}/${tarball} --output ${TARBALL_PATH}
fi

if [ ! -f ${TARBALL_PATH} ]
then
    echo "Error! nodejs tarball ${TARBALL_PATH} not found"
    exit 9
fi


if [ 'X'${INSTALL_HOME} == 'X' ]
then
    INSTALL_HOME='/usr/local/lib/nodejs'
    echo "use default nodejs install home: ${INSTALL_HOME}"    
fi
if [ -d ${INSTALL_HOME} ]
then
    echo "Error! nodejs install dir ${INSTALL_HOME} exists, should clean nodejs path before re-install..."
    exit 5
fi

mkdir -p ${INSTALL_HOME}


is_centos=`awk -F '=' '/PRETTY_NAME/ { print $2 }' /etc/os-release | grep -i centos | wc -l`
is_ubuntu=`awk -F '=' '/PRETTY_NAME/ { print $2 }' /etc/os-release | grep -i ubuntu | wc -l`

install_libs_centos(){
    yum update -y
    yum install libgcc -y
    yum install libstdc++ -y
}

install_libs_ubuntu(){
    apt-get update -y
    # apt-get install libstdc++6 -y
}

if [ $is_centos -gt 0 ]
then
    # install some libs
    install_libs_centos
fi

if [ $is_ubuntu -gt 0 ]
then
    # install some libs
    install_libs_ubuntu
fi

cd /tmp
xz -dc ${TARBALL_PATH} | tar -x  -C ${INSTALL_HOME} --strip-components 1

chmod -R 755 ${INSTALL_HOME}


# add ln for "node" "npm" "npx"
if [ -e /usr/bin/node -o -L /usr/bin/node ]
then
    rm -rf /usr/bin/node
fi
ln -s ${INSTALL_HOME}/bin/node /usr/bin/node

if [ -e /usr/bin/npm -o -L /usr/bin/npm ]
then
    rm -rf /usr/bin/npm
fi
ln -s ${INSTALL_HOME}/bin/npm /usr/bin/npm

if [ -e /usr/bin/npx -o -L /usr/bin/npx ]
then
    rm -rf /usr/bin/npx
fi
ln -s ${INSTALL_HOME}/bin/npx /usr/bin/npx


echo "install nodejs in ${INSTALL_HOME} completed"


if [ -e /usr/bin/cnpm -o -L /usr/bin/cnpm ]
then
    rm -rf /usr/bin/cnpm
fi
npm install -g cnpm --registry=https://registry.npmmirror.com
ln -s ${INSTALL_HOME}/bin/cnpm /usr/bin/cnpm

chown -R root:root ${INSTALL_HOME}
chmod -R 755 ${INSTALL_HOME}

echo "install cnpm completed"