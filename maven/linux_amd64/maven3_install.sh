#!/bin/bash

run_user=`whoami`
if [ 'X'$run_user != 'X''root' ]
then
    echo 'Error! should run me with root...'
    exit 1
fi

# absolute path of maven tarball used. if not give, will download maven tarball from Internet
TARBALL_PATH=''

# absolute path to install maven
INSTALL_HOME=''


usage() { 
  echo -e "
Usage:
  $0 [-f ARCHIVE_TARBARR_PATH] [-h INSTALL_HOME]
Common options:
  -f,  file, absolute path of archive tarball. Without -f options will download tarball from Internet
  -h,  home, absolute path of jdk install path, default to "'`/usr/local/maven`'"
" 1>&2; exit 1;
}

while getopts ":f:h:" o; do
    case "${o}" in
        f)
            v=${OPTARG}
            if [ 'X'$v == 'X' ]
            then
              usage
            else
              TARBALL_PATH=${OPTARG}
            fi
            ;;
        h)
            v=${OPTARG}
            if [ 'X'$v == 'X' ]
            then
              usage
            else
              INSTALL_HOME=${OPTARG}
            fi
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))


if [ 'X'${TARBALL_PATH} == 'X' ]
then
    # version used when no tarball give
    DEFAULT_VERSION=3.8.7

    tarball="apache-maven-${DEFAULT_VERSION}-bin.tar.gz"
    TARBALL_PATH="/tmp/${tarball}"
    if [ -f ${TARBALL_PATH} ]
    then
        rm -rf ${TARBALL_PATH}
    fi
    echo "not give maven tarball absolute path, will download ${tarball} from Internet"
    # https://dlcdn.apache.org/maven/maven-3/3.8.7/binaries/apache-maven-3.8.7-bin.tar.gz
    curl https://dlcdn.apache.org/maven/maven-3/${DEFAULT_VERSION}/binaries/${tarball} --output ${TARBALL_PATH}
fi

if [ ! -f ${TARBALL_PATH} ]
then
    echo "Error! maven tarball ${TARBALL_PATH} not found"
    exit 9
fi


if [ 'X'${INSTALL_HOME} == 'X' ]
then
    INSTALL_HOME='/usr/local/maven'
    echo "use default maven install home: ${INSTALL_HOME}"    
fi
if [ -d ${INSTALL_HOME} ]
then
    echo "Error! maven install dir ${INSTALL_HOME} exists, should clean maven path before re-install..."
    exit 5
fi

mkdir -p ${INSTALL_HOME}

cd /tmp
tar -xzvf ${TARBALL_PATH} -C ${INSTALL_HOME} --strip-components 1

chmod -R 755 ${INSTALL_HOME}

# add MAVEN_HOME env to /etc/profile
# grep if previous env exists
cnt=`cat /etc/profile | grep "^\s*MAVEN_HOME" | wc -l`
if [ $cnt -lt 1 ]
then
    echo "MAVEN_HOME=${INSTALL_HOME}" >> /etc/profile
else
    # if previous env exists just replace the content
    # slash '/'' is in the variables, so use different separator '|'
    sed -i "s|^\s*MAVEN_HOME=.*|MAVEN_HOME=${INSTALL_HOME}|g" /etc/profile
fi

# grep if previous env export exists
cnt=`cat /etc/profile | grep "^\s*export MAVEN_HOME" | wc -l`
if [ $cnt -lt 1 ]
then
    echo "export MAVEN_HOME" >> /etc/profile
else
    # if export exists, remove it and add new export expr at last line
    cat /etc/profile | grep -v "^\s*export MAVEN_HOME" > /etc/profile_new
    cat /etc/profile_new > /etc/profile
    echo "export MAVEN_HOME" >> /etc/profile
fi

source /etc/profile

# add ln for "mvn"
if [ -e /usr/bin/mvn -o -L /usr/bin/mvn ]
then
    rm -rf /usr/bin/mvn
fi

ln -s ${INSTALL_HOME}/bin/mvn /usr/bin/mvn

echo "install mvn in ${INSTALL_HOME} completed, can run 'mvn -v' and check. if you want to use MAVEN_HOME in current bash, may need to execute 'source /etc/profile'"
