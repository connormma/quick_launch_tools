#!/bin/bash

run_user=`whoami`

if [ 'X'$run_user != 'X''root' ]
then
    echo 'Error! should run me with root...'
    exit 1
fi

# absolute path of php tarball used. if not give, will download php tarball from Internet
TARBALL_PATH=''

# absolute path to install php
INSTALL_HOME=''


usage() { 
  echo -e "
Usage:
  $0 [-f ARCHIVE_TARBARR_PATH] [-h INSTALL_HOME]
Common options:
  -f,  file, absolute path of archive tarball. Without -f options will download tarball from Internet
  -h,  home, absolute path of php install path, default to "'`/usr/local/php`'"
" 1>&2; exit 1;
}

while getopts ":f:h:" o; do
    case "${o}" in
        f)
            v=${OPTARG}
            if [ 'X'$v == 'X' ]
            then
              usage
            else
              TARBALL_PATH=${OPTARG}
            fi
            ;;
        h)
            v=${OPTARG}
            if [ 'X'$v == 'X' ]
            then
              usage
            else
              INSTALL_HOME=${OPTARG}
            fi
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))


if [ 'X'${TARBALL_PATH} == 'X' ]
then
    # version used when no tarball give
    DEFAULT_VERSION=7.2.34

    tarball="php-${DEFAULT_VERSION}.tar.gz"
    TARBALL_PATH="/tmp/${tarball}"

    if [ -f ${TARBALL_PATH} ]
    then
        rm -rf ${TARBALL_PATH}
    fi
    echo "not give php tarball absolute path, will download ${tarball} from Internet"
    # https://www.php.net/distributions/php-7.2.34.tar.gz
    curl https://www.php.net/distributions/${tarball} --output ${TARBALL_PATH}
fi
if [ ! -f ${TARBALL_PATH} ]
then
    echo "ERROR! php tarball:${TARBALL_PATH} not found, exit..."
    exit 11
fi

if [ 'X'${INSTALL_HOME} == 'X' ]
then
    INSTALL_HOME='/usr/local/php'
    echo "not give php install home absolute path, will use default path: ${INSTALL_HOME}"
fi
if [ -d ${INSTALL_HOME} ]
then
    echo "ERROR! php home:${INSTALL_HOME} exists, exit..."
    exit 10
fi


remove_php_centos(){
    cd /
    # stop php-fpm process
    systemctl disable php-fpm.service 
    systemctl stop php-fpm.service 
    
    # remove php* with yum
    yum remove *php* -y

    rm -f /etc/systemd/system/php-fpm.service
    systemctl daemon-reload
    
    # del previous php user: www
    userdel www
    groupdel www
}

remove_php_ubuntu(){
    cd /
    # stop php process
    systemctl disable php-fpm.service 
    systemctl stop php-fpm.service 
    
    # remove php* with apt-get
    apt-get remove *php* -y

    rm -f /etc/systemd/system/php-fpm.service
    systemctl daemon-reload
    
    # del previous php user: www
    userdel www
    groupdel www
}

install_libs_centos(){
    cd /
    yum update -y
    # install gcc, g++ and make
    yum install gcc gcc-c++ make -y
    
    yum -y install libxml2-devel libjpeg-devel libpng-devel freetype-devel curl-devel openssl-devel zlib-devel
}

install_libs_ubuntu(){
    cd /
    apt-get update -y
    # install gcc, g++ and make
    apt-get install build-essential -y 
    # apt-get install make -y 

    apt-get install -y libxml2-dev libjpeg-dev libpng-dev libfreetype6-dev libcurl4-openssl-dev openssl libssl-dev zlib1g-dev
}

is_centos=`awk -F '=' '/PRETTY_NAME/ { print $2 }' /etc/os-release | grep -i centos | wc -l`
is_ubuntu=`awk -F '=' '/PRETTY_NAME/ { print $2 }' /etc/os-release | grep -i ubuntu | wc -l`


if [ $is_centos -gt 0 ]
then
    # stop php-fpm process, and del previous php user
    remove_php_centos
    # install libs
    install_libs_centos
    # create user www:www
    adduser www --system --no-create-home --shell /bin/false --user-group
fi

if [ $is_ubuntu -gt 0 ]
then
    # stop php-fpm process, and del previous php user
    remove_php_ubuntu
    # install libs
    install_libs_ubuntu
    # create user www:www
    adduser www --system --no-create-home --shell /bin/false --group
fi


cd /tmp
if [ -d /tmp/php_packages ]
then
    rm -rf /tmp/php_packages
fi

mkdir -p /tmp/php_packages

if [ -d ${INSTALL_HOME} ]
then
    rm -rf ${INSTALL_HOME}
fi
mkdir -p ${INSTALL_HOME}

chmod -R 755 ${INSTALL_HOME}

cd /tmp/php_packages
tar -xzvf ${TARBALL_PATH} -C /tmp/php_packages --strip-components 1

make distclean

./configure \
--prefix=${INSTALL_HOME} \
--with-config-file-path=${INSTALL_HOME}/etc \
--enable-fpm \
--with-fpm-user=www \
--with-fpm-group=www \
--with-curl \
--with-openssl \
--with-zlib \
--enable-pdo \
--with-mysqli=mysqlnd \
--with-pdo-mysql=mysqlnd \
--enable-mysqlnd \
--enable-bcmath \
--enable-ftp \
--enable-sockets \
--enable-wddx \
--enable-mbstring \
--enable-dom \
--enable-pcntl \
--enable-shmop \
--enable-soap

make install

# exit with error if make php fail
ret=$?

if [ 'X'$ret != 'X'0 ]
then
    make distclean
    rm -rf /tmp/php_packages
    echo 'Error! make php failed...'
    exit 9
fi

cd /tmp/php_packages
cp /tmp/php_packages/php.ini-production ${INSTALL_HOME}/etc/php.ini

cd ${INSTALL_HOME}/etc/
cp ${INSTALL_HOME}/etc/php-fpm.conf.default ${INSTALL_HOME}/etc/php-fpm.conf
cd ${INSTALL_HOME}/etc/php-fpm.d
cp ${INSTALL_HOME}/etc/php-fpm.d/www.conf.default ${INSTALL_HOME}/etc/php-fpm.d/www.conf

rm -rf /tmp/php_packages

chown -R www:www ${INSTALL_HOME}

# add ln for "php"
if [ -e /usr/bin/php -o -L /usr/bin/php ]
then
    rm -rf /usr/bin/php
fi
ln -s ${INSTALL_HOME}/bin/php /usr/bin/php


echo "
[Unit]
Description=The PHP FastCGI Process Manager
After=syslog.target network.target
 
[Service]
PIDFile=${INSTALL_HOME}/var/run/php-fpm.pid
ExecStart=${INSTALL_HOME}/sbin/php-fpm --nodaemonize
ExecReload=/bin/kill -USR2 $MAINPID
PrivateTmp=true
 
[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/php-fpm.service


rm -rf /tmp/php_packages

echo "php installed in ${INSTALL_HOME}, conf file in ${INSTALL_HOME}/etc"

systemctl daemon-reload
systemctl enable php-fpm.service 
systemctl start php-fpm

echo "php installed, and can use 'systemctl status php-fpm' to check php fpm status"